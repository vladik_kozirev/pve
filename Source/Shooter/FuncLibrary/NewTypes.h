// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Chaos/ChaosEngineInterface.h"
#include "Engine/DataTable.h"
#include "NewTypes.generated.h"


UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName = "Aim State"),
	AimWalk_State UMETA(DisplayName = "AimWalk State"),
	Walk_State UMETA(DisplayName = "Walk"),
	Run_State UMETA(DisplayName = "Run State"),
	Sprint_State UMETA(DisplayName = "Sprint State")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		float AimSpeed = 300.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		float AimWalkSpeed = 400.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		float WalkSpeed = 400.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		float RunSpeed = 600.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		float SprintSpeed = 800.0f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
		TSubclassOf<class AProjectileDefault> Projectile = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float ProjectileDamage = 20.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float ProjectileLifeTime = 20.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float ProjectileInitSpeed = 2000.0f;
	//Material to decal on hit
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
		TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
		USoundBase* HitSound = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
		TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
		UParticleSystem* ExploseFX = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
		USoundBase* ExploseSound = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float ExploseMaxDamage = 40.0f;

	//Hit fx actor
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
		bool bIsLikeBomp = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float InnerRadiusDamage = 200.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float OuterRadiusDamage = 400.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float CoefReductionDamage = 5.0f;
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float Aim_StateDispersionAimMax = 2.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float Aim_StateDispersionAimMin = 0.3f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float Aim_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float Aim_StateDispersionReduction = 0.3f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float AimWalk_StateDispersionAimMax = 3.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float AimWalk_StateDispersionAimMin = 0.04f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float AimWalk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float AimWalk_StateDispersionReduction = 0.4f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float Walk_StateDispersionMax = 1.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float Walk_StateDispersionMin = 0.1f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float Walk_StateDispersionRecoil = 1.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float Walk_StateDispersionReduction = 1.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float Run_StateDispersionMax = 10.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float Run_StateDispersionMin = 4.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float Run_StateDispersionRecoil = 1.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		float Run_StateDispersionReduction = 0.1f;
};

USTRUCT(BlueprintType)
struct FAnimationWeaponInfo
{
	GENERATED_BODY()
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AnimChar")
		UAnimMontage* AnimCharFire = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AnimChar")
		UAnimMontage* AnimCharFireAim = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AnimChar")
		UAnimMontage* AnimCharReload = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AnimChar")
		UAnimMontage* AnimCharReloadAim = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AnimWeapon")
		UAnimMontage* AnimWeaponReload = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AnimWeapon")
		UAnimMontage* AnimWeaponReloadAim = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AnimWeapon")
		UAnimMontage* AnimWeaponFire = nullptr;
};

USTRUCT(BlueprintType)
struct FDropMeshInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "DropMesh")
		UStaticMesh* DropMesh = nullptr; 
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "DropMesh")
		float DropMeshTime = -1.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "DropMesh")
		float DropMeshLifeTime = 5.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "DropMesh")
		FTransform DropMeshOffset = FTransform();
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "DropMesh")
		FVector DropMeshImpulseDir = FVector(0.0f);
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "DropMesh")
		float PowerImpulse = 0.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "DropMesh")
		float ImpulseRandomDispersion = 0.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "DropMesh")
		float CustomMass = 0.0f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Class")
		TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "State")
		float RateOfFire = 0.5f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "State")
		float ReloadTime = 2.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "State")
		int32 MaxRound = 10;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "State")
		int32 NumberProjectileByShot = 1;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		FWeaponDispersion WeaponDispersion;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundReloadWeapon = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "FX")
		UParticleSystem* EffectFireWeapon = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Projectile")
		FProjectileInfo ProjectileSetting;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Trace")
		float WeaponDamage = 20.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Trace")
		float DistanceTrace = 2000.0f;
	//one decal on all
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "HitEffect")
		UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Anim")
		FAnimationWeaponInfo AnimWeaponInfo;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Mesh")
		FDropMeshInfo ClipDropMesh;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Mesh")
		FDropMeshInfo ShellBullets;
};

USTRUCT(BlueprintType)
struct FAddicionalWeaponInfo
{
	GENERATED_BODY()

		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "WeaponStats")
		int32 Round = 10;
};

UCLASS()
class SHOOTER_API UNewTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};