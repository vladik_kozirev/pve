// Fill out your copyright notice in the Description page of Project Settings.


#include "Shooter/Weapon/ProjectileDefault_Grenade.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);
}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			Explose();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, 
														  UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose() //Debug Sphher !!!  //WeaponDefault.cpp LinetraceSignaleByChanel() �� ��������� projectileDefault
{
	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
	}
	if (ProjectileSetting.bIsLikeBomp)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.InnerRadiusDamage, 12, FColor::Red, false, 3.0f, 0, 2.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.OuterRadiusDamage, 12, FColor::Blue, false, 3.0f, 0, 2.0f);
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), ProjectileSetting.ExploseMaxDamage, 
												ProjectileSetting.ExploseMaxDamage * 0.2f, GetActorLocation(), ProjectileSetting.InnerRadiusDamage, ProjectileSetting.OuterRadiusDamage,
												5, NULL, IgnoredActor, nullptr, nullptr);
	this->Destroy();
}
