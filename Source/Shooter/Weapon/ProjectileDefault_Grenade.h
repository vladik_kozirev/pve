// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Shooter/Weapon/ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()
	
protected:
	//Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	//Called evry frame
	virtual void Tick(float DeltaTime) override;

	void TimerExplose(float DeltaTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor,
												UPrimitiveComponent* OtherComp, FVector NormalImpulse,
												const FHitResult& Hit);

	virtual void ImpactProjectile() override;

	UFUNCTION()
	void Explose();

	bool TimerEnabled = false;
	float TimerToExplose = 0.0f;
	float TimeToExplose = 4.0f;
};
